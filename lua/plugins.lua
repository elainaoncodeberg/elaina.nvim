vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerCompile
  augroup end
]])

return require('packer').startup(function(use) 
    use 'wbthomason/packer.nvim'
    use {
        'nvim-treesitter/nvim-treesitter',
        run = ':TSUpdate',
        config = function() require 'pluginsCfg.treesitter' end
    }
    use 'windwp/nvim-ts-autotag'
    use {
        'williamboman/mason.nvim',
        config = function() require 'pluginsCfg.mason' end
        }
    use 'williamboman/mason-lspconfig.nvim'
    use 'neovim/nvim-lspconfig'
    use 'jose-elias-alvarez/null-ls.nvim'

    -- use {
    --     'neovim/nvim-lspconfig',
    --     config = function() require 'pluginsCfg.lspconfig' end
    -- }
    use {
        'hrsh7th/nvim-cmp',
        config = function() require 'pluginsCfg.nvim-cmp' end
    }
    use 'hrsh7th/cmp-path'
    use 'hrsh7th/cmp-nvim-lsp'
    use 'hrsh7th/cmp-buffer'
    use 'saadparwaiz1/cmp_luasnip'
    use {
        'L3MON4D3/LuaSnip',
        config = function() require 'pluginsCfg.LuaSnip' end
    }
    use 'jiangmiao/auto-pairs'
    use {
        'lukas-reineke/indent-blankline.nvim',
        opt = true,
        event = 'BufWinEnter',
        config = function() require 'indent_blankline'.setup {
            char_highlight_list = {
                'IndentBlanklineIndent',
            }
        } end
    }
    use 'kyazdani42/nvim-web-devicons'
    use {
        'kyazdani42/nvim-tree.lua',
        opt = true,
        cmd = {
            'NvimTreeToggle',
        },
        config = function() require 'nvim-tree'.setup {} end
    }
    use {
        'terrortylor/nvim-comment',
        opt = true,
        cmd = {
            'CommentToggle',
        },
        config = function() require('nvim_comment').setup {
            create_mappings = false
        } end
    }
    use {
        'nvim-lualine/lualine.nvim',
        config = function() require 'pluginsCfg.lualine' end
    }
    use {
        'akinsho/bufferline.nvim',
        config = function() require 'pluginsCfg.bufferline' end
    }
    use 'joshdick/onedark.vim'
    use {
        'norcalli/nvim-colorizer.lua',
        config = function() require 'colorizer'.setup{} end
    }
end)

