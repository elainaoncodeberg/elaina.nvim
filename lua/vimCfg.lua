--ui
vim.cmd('colorscheme onedark')
vim.o.number = true
vim.o.tabstop = 4
vim.o.shiftwidth = 4
vim.o.expandtab = true
vim.o.wrap = false
vim.o.mouse = 'a'
vim.o.showtabline = 3
vim.go.termguicolors = true
vim.cmd([[
    hi Normal guibg=NONE ctermbg=NONE
    hi Comment gui=italic cterm=italic 
    hi lualine_c_normal guibg=NONE
    hi lualine_c_inactive guibg=NONE
]])

--keybindings
local key = vim.api.nvim_set_keymap
key('','<C-Space>',':CommentToggle<CR>',{noremap = true,silent = true})
key('','<C-n>',':NvimTreeToggle<CR>',{noremap = true,silent = true})
key('','<TAB>',':bn<CR>',{noremap = true,silent = true})
key('','<TAB>',':bn<CR>',{noremap = true,silent = true})
vim.cmd([[
    nnoremap <C-h> <C-w>h
    nnoremap <C-j> <C-w>j
    nnoremap <C-k> <C-w>k
    nnoremap <C-l> <C-w>l
]])
