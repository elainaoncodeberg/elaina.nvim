require('lualine').setup {
    options = {
        theme = 'auto',
        component_separators = { left = '', right = ''},
        section_separators = { left = '', right = ''},
    },
    sections = {
        lualine_y = {},
    },
    inactive_sections = {
        lualine_x = {},
    },
}



